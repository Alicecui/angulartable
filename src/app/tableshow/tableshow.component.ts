import { Component, OnInit } from '@angular/core';
import {MService} from '../m.service';


@Component({
  selector: 'app-tableshow',
  templateUrl: './tableshow.component.html',
  styleUrls: ['./tableshow.component.css']
})
export class TableshowComponent implements OnInit {
  p = 1;
  infors;
  constructor(public mService: MService) {
  }
  ngOnInit() {
    this.mService.getData().subscribe((item: any) => {
    this.infors = item;
  });
  }
}
