import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MService} from './m.service';
import { AppComponent } from './app.component';
import { TableshowComponent } from './tableshow/tableshow.component';
import { HttpClientModule} from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [
    AppComponent,
    TableshowComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [MService],
  bootstrap: [AppComponent]
})
export class AppModule { }
