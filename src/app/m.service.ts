import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MService {



  constructor(public http: HttpClient) {
  }

  getData( ) {
    return this.http.get('assets/data/sample_data.json');
  }
}
